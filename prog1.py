#! asynchronous headline retriever

from twisted.internet import reactor,defer


class HeadLineRetriever(object):

	def processHeadLine(self,headline):
		if len(headline) > 50:
			self.d.errback("The headline '%s' is too long" %(headline))
		else:
			self.d.callback(headline)

	def _toHTML(self,result):
		return "<h1>%s</h1>" %(result)

	def getHeadLine(self,line):
		self.d=defer.Deferred()
		reactor.callLater(1,self.processHeadLine,line)
		self.d.addCallback(self._toHTML)
		return self.d

def printData(result):
	print result
	reactor.stop()

def printError(failure):
	print failure
	reactor.stop()

h=HeadLineRetriever()
d=h.getHeadLine("AAAAAAAAAAAAAAA AAAAAAAAAA AAAAAAAAAAAA AAAAAAAAAAAA AAAAAAAAAA")
d.addCallbacks(printData,printError)

reactor.run()
