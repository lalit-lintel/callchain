#! various function for use as callbacks and errorbacks

from twisted.internet.defer import Deferred

def callback1(result):
	print "callback 1 said : ",result
	return result

def callback2(result):
	print "callback 2 said : ",result

def callback3(result):
	raise Exception("callback 3")

def errback1(failure):
	print "Errback 1 had an an error on",failure
	return failure

def errback2(failure):
	raise Exception("Errorback 2")

def errback3(failure):
	print "errback take care of",failure
	return "Everything is fine now."

# Ex 1
"""
d=Deferred()
d.addCallback(callback1)
d.addCallback(callback2)
d.callback("test")
"""

# Ex 2
"""
d=Deferred()
d.addCallback(callback1)
d.addCallback(callback2)
d.addCallback(callback3)
d.addErrback(errback3)
d.callback("test")
"""

# Ex 3
"""
d=Deferred()
d.addErrback(errback2)
#d.addErrback(errback3)
d.errback("test")
"""

#Ex 4

d=Deferred()
d.addCallback(callback3)
d.addCallbacks(callback2,errback3)
d.addCallbacks(callback1,errback2)
d.callback("test")
